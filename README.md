# Teamwork-WIM

This is a **Work Item Manegment system** developed for a Teamwork Assigment by [**TelerikAcademy**](https://www.telerikacademy.com).

Basic system for managing **Teams** which contain **Members**. **Teams** and **Members** can have a **Board** for organization for their **Work** Item****s which could be **Bugs**, **Stories** and **Feedback**.

# Running Tests

The application has basic **Unit Tests** for testing it's **Constructors** & **Validations** for properties of classes with given field constrains.

# Built With

 - Java 8

# Developers
**Team 12**:
 - [Daniel Galabov](https://gitlab.com/danielgylybov)
 - [Emil Koev](https://gitlab.com/Emo98)
# Links
 - [**Our Trello Board**](https://trello.com/b/RTxLWCih/work-item-manegment-system)