package Teamwork;

import Teamwork.Engine.EngineImpl;

public class Demo {

    public static void main(String[] args) {
        EngineImpl engine = new EngineImpl();
        engine.start();
    }
}
