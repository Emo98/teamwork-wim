package Teamwork.Engine;

import java.util.ArrayList;

class EngineConstants {

    // Commands
    static final String CreateNewPerson = "Create a new Member";
    static final String ShowAllPeople = "Show all people";
    static final String ShowPersonActivity = "Show person's activity ";
    static final String CreateNewTeam = "Create a new team";
    static final String ShowAllTeams = "Show all teams";
    static final String ShowTeamsActivity = "Show team's activity";
    static final String ShowAllTeamMembers = "Show all team members";
    static final String CreateNewBoardInTeam = "Create a new board in a team";
    static final String ShowAllTeamBoards = "Show all team boards";
    static final String ShowBoardActivity = "Show board's activity";
    static final String CreateNewBugInBoard = "Create a new Bug in a board";
    static final String CreateNewStoryInBoard = "Create a new Story in a board";
    static final String CreateNewFeedbackInBoard = "Create a new Feedback in a board";
    static final String ChangePriorityOfAWorkItem = "Change Priority Level of a Work Item";
    static final String ChangeSeverityOfABug = "Change Severity of a Bug";
    static final String AddStepsOfReproduction = "Add Steps of Reproduction to a Bug";
    static final String ChangeSizeOfAStory = "Change Size of a Story";
    static final String ChangeRatingOfAFeedback = "Change Rating of Feedback";
    static final String ChangeStatusOfAWorkItem = "Change Status of a Work Item";
    static final String ChangeWorkItemAssign = "Change Work Item assignee";
    static final String AddCommentToAWorkItem = "Add comment to a Work Item";
    static final String ShowCommands = "Show all commands";
    static final String ListWorkItems = "Filter/Sort Work Items";
    static final String Exit = "Exit";

    //output messages
    static final String INVALID_COMMAND = "Invalid Command!";
    static final String RATING_CHANGED = "Rating changed!";
    static final String SIZE_CHANGED = "Size changed!";
    static final String SEVERITY_CHANGED = "Severity changed!";
    static final String STATUS_CHANGED = "Status changed!";
    static final String PRIORITY_CHANGED = "Priority Level changed!";
    static final String ASSIGN_CHANGED = "Assign changed!";
    static final String COMMENT_ADDED = "Comment added!";
    static final String STORY_CREATED = "Story created!";
    static final String FEEDBACK_CREATED = "Feedback created!";
    static final String BUG_CREATED = "Bug created!";
    static final String BOARD_CREATED = "Board created!";
    static final String MEMBER_CREATED = "Member created!";
    static final String TEAM_CREATED = "Team created!";
    static final String STEPS_ADDED = "Steps of reproduction added!";


    static ArrayList<String> getFirstCommands() {
        ArrayList<String> firstCommands = new ArrayList<>();
        firstCommands.add(Exit);
        firstCommands.add(ShowCommands);
        firstCommands.add(CreateNewTeam);

        return firstCommands;
    }

    static void addTeamCommands(ArrayList<String> commands) {
        if (commands.contains(CreateNewBoardInTeam)) {
            return;
        }
        commands.add(CreateNewBoardInTeam);
        commands.add(CreateNewPerson);
        commands.add(ShowAllTeams);
        commands.add(ShowAllPeople);
        commands.add(ShowAllTeamBoards);
        commands.add(ShowAllTeamMembers);

    }

    static void addBoardAndMemberCommands(ArrayList<String> commands) {
        if (commands.contains(CreateNewBugInBoard)) {
            return;
        }
        commands.add(CreateNewBugInBoard);
        commands.add(CreateNewStoryInBoard);
        commands.add(CreateNewFeedbackInBoard);

    }

    static void addWorkItemCommands(ArrayList<String> commands) {
        if (commands.contains(ShowTeamsActivity)) {
            return;
        }
        commands.add(ShowTeamsActivity);
        commands.add(ShowBoardActivity);
        commands.add(ShowPersonActivity);
        commands.add(AddCommentToAWorkItem);
        commands.add(ChangeWorkItemAssign);
        commands.add(ChangeStatusOfAWorkItem);
        commands.add(ListWorkItems);

    }

    static void addBugCommands(ArrayList<String> commands) {
        if (commands.contains(ChangeSeverityOfABug)) {
            return;
        }
        commands.add(ChangeSeverityOfABug);
        commands.add(AddStepsOfReproduction);

    }

    static void addStoryCommands(ArrayList<String> commands) {
        if (commands.contains(ChangeSizeOfAStory)) {
            return;
        }
        commands.add(ChangeSizeOfAStory);

    }

    static void addFeedBackCommands(ArrayList<String> commands) {
        if (commands.contains(ChangeRatingOfAFeedback)) {
            return;
        }
        commands.add(ChangeRatingOfAFeedback);

    }

    static void addBugAndStoryBaseCommands(ArrayList<String> commands) {
        if (commands.contains(ChangePriorityOfAWorkItem)) {
            return;
        }
        commands.add(ChangePriorityOfAWorkItem);

    }
}
