package Teamwork.Engine;

import Teamwork.WorkItemManagement.BoardImpl;
import Teamwork.WorkItemManagement.MemberImpl;
import Teamwork.WorkItemManagement.WorkItems.contracts.*;
import Teamwork.WorkItemManagement.contracts.Team;

import java.util.*;

public class EngineImpl extends EngineProcessor {

    private ArrayList<String> availableCommands = new ArrayList<>();

    public EngineImpl() {
        super();
    }

    public void start() {
        availableCommands = EngineConstants.getFirstCommands();
        while (true) {
            if (availableCommands.size() != checkCommands().size()) {
                System.out.println("There is a change in the Command List!");
                availableCommands = checkCommands();
            }
            int command = chooseByIndex(availableCommands);
            try {

                String output = processCommands(command);
                if (output.equals(EngineConstants.Exit)) {
                    break;
                }
                System.out.println(output);
                System.out.println("Click 'Enter' to continue...");
                scanner.nextLine();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }


    private String processCommands(int command) {
        String outputMessage;

        Team team;
        BoardImpl board;
        MemberImpl member;
        WorkItem workItem;

        switch (availableCommands.get(command)) {
            case EngineConstants.CreateNewTeam:
                team = createNewTeam();
                allTeams.put(team.getName(), team);
                outputMessage = EngineConstants.TEAM_CREATED;
                break;
            case EngineConstants.CreateNewPerson:
                member = createNewPerson();
                allMembers.put(member.getName(), member);
                outputMessage = EngineConstants.MEMBER_CREATED;
                break;
            case EngineConstants.CreateNewBoardInTeam:
                board = createNewBoard();
                allBoards.put(board.getName(), board);
                outputMessage = EngineConstants.BOARD_CREATED;
                break;
            case EngineConstants.CreateNewBugInBoard:
                workItem = createWorkItem(BUG);
                allWorkItems.put(workItem.getId(), workItem);
                outputMessage = EngineConstants.BUG_CREATED;
                break;
            case EngineConstants.CreateNewFeedbackInBoard:
                workItem = createWorkItem(FEEDBACK);
                allWorkItems.put(workItem.getId(), workItem);
                outputMessage = EngineConstants.FEEDBACK_CREATED;
                break;
            case EngineConstants.CreateNewStoryInBoard:
                workItem = createWorkItem(STORY);
                allWorkItems.put(workItem.getId(), workItem);
                outputMessage = EngineConstants.STORY_CREATED;
                break;
            case EngineConstants.ShowAllPeople:
                outputMessage = getAllKeysFromMap(allMembers);
                break;
            case EngineConstants.ShowAllTeamBoards:
                outputMessage = getAllTeamBoards();
                break;
            case EngineConstants.ShowAllTeamMembers:
                outputMessage = getAllTeamMembers();
                break;
            case EngineConstants.ShowAllTeams:
                outputMessage = getAllKeysFromMap(allTeams);
                break;
            case EngineConstants.ShowPersonActivity:
                outputMessage = getPersonActivity();
                break;
            case EngineConstants.ShowBoardActivity:
                outputMessage = getBoardActivity();
                break;
            case EngineConstants.ShowTeamsActivity:
                outputMessage = getTeamsActivity();
                break;
            case EngineConstants.AddCommentToAWorkItem:
                addCommentToAWorkItem();
                outputMessage = EngineConstants.COMMENT_ADDED;
                break;
            case EngineConstants.ChangeWorkItemAssign:
                changeWorkItemAssignee();
                outputMessage = EngineConstants.ASSIGN_CHANGED;
                break;
            case EngineConstants.ChangePriorityOfAWorkItem:
                changePriorityOfAWorkItem();
                outputMessage = EngineConstants.PRIORITY_CHANGED;
                break;
            case EngineConstants.ChangeStatusOfAWorkItem:
                changeStatusOfAWorkItem();
                outputMessage = EngineConstants.STATUS_CHANGED;
                break;
            case EngineConstants.ChangeSeverityOfABug:
                changeSeverityOfABug();
                outputMessage = EngineConstants.SEVERITY_CHANGED;
                break;
            case EngineConstants.AddStepsOfReproduction:
                addStepsOfReproduction();
                outputMessage = EngineConstants.STEPS_ADDED;
                break;
            case EngineConstants.ChangeSizeOfAStory:
                changeSizeOfAStory();
                outputMessage = EngineConstants.SIZE_CHANGED;
                break;
            case EngineConstants.ChangeRatingOfAFeedback:
                changeRatingOfAFeedback();
                outputMessage = EngineConstants.RATING_CHANGED;
                break;
            case EngineConstants.ShowCommands:
                outputMessage = listToString(availableCommands);
                break;
            case EngineConstants.ListWorkItems:
                outputMessage = ListAndFilterWorkItems();
                break;
            case EngineConstants.Exit:
                outputMessage = EngineConstants.Exit;
                break;
            default:
                outputMessage = EngineConstants.INVALID_COMMAND;
                break;
        }
        return outputMessage;
    }


}