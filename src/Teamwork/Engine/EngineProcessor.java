package Teamwork.Engine;

import Teamwork.WorkItemManagement.BoardImpl;
import Teamwork.WorkItemManagement.Common.*;
import Teamwork.WorkItemManagement.MemberImpl;
import Teamwork.WorkItemManagement.TeamImpl;
import Teamwork.WorkItemManagement.WorkItems.BugImpl;
import Teamwork.WorkItemManagement.WorkItems.FeedbackImpl;
import Teamwork.WorkItemManagement.WorkItems.PriorityItemBase;
import Teamwork.WorkItemManagement.WorkItems.StoryImpl;
import Teamwork.WorkItemManagement.WorkItems.contracts.*;
import Teamwork.WorkItemManagement.contracts.Team;

import java.util.*;
import java.util.stream.Collectors;

class EngineProcessor {
    static final String BUG = "Bug";
    static final String STORY = "Story";
    static final String FEEDBACK = "Feedback";
    private static final String ALL = "ALL";
    private static final String TITLE = "Title";
    private static final String PRIORITY = "Priority Level";
    private static final String SEVERITY = "Severity";
    private static final String SIZE = "Size";
    private static final String RATING = "Rating";
    private static final String ASSIGNABLE = "Assignable";


    Scanner scanner = new Scanner(System.in);
    Map<String, MemberImpl> allMembers;
    Map<String, BoardImpl> allBoards;
    Map<Integer, WorkItem> allWorkItems;
    Map<String, Team> allTeams;

    EngineProcessor() {
        allMembers = new HashMap<>();
        allBoards = new HashMap<>();
        allWorkItems = new HashMap<>();
        allTeams = new HashMap<>();
    }

    ArrayList<String> checkCommands() {
        ArrayList<String> newCommands;
        boolean isAnyTeamReadyForWork = false;
        for (Map.Entry<String, Team> entry : allTeams.entrySet()) {
            if (entry.getValue().isReadyForWork()) {
                isAnyTeamReadyForWork = true;
                break;
            }
        }
        newCommands = EngineConstants.getFirstCommands();
        if (allTeams.size() > 0) {
            EngineConstants.addTeamCommands(newCommands);
        }

        if (isAnyTeamReadyForWork) {
            EngineConstants.addBoardAndMemberCommands(newCommands);
        }
        if (allWorkItems.size() > 0) {
            EngineConstants.addWorkItemCommands(newCommands);
        }
        if (allWorkItems.entrySet().stream().filter(x ->
                (x.getValue() instanceof BugImpl))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).size() > 0) {

            EngineConstants.addBugAndStoryBaseCommands(newCommands);
            EngineConstants.addBugCommands(newCommands);
        }
        if (allWorkItems.entrySet().stream().filter(x ->
                (x.getValue() instanceof StoryImpl))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).size() > 0) {

            EngineConstants.addBugAndStoryBaseCommands(newCommands);
            EngineConstants.addStoryCommands(newCommands);
        }
        if (allWorkItems.entrySet().stream().filter(x ->
                (x.getValue() instanceof FeedbackImpl))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).size() > 0) {

            EngineConstants.addFeedBackCommands(newCommands);
        }

        return newCommands;
    }

    Team createNewTeam() {
        System.out.print("Enter a Team name: ");
        String name = scanner.nextLine();
        checkForName(allTeams, name);
        return new TeamImpl(name);
    }

    BoardImpl createNewBoard() {
        Team team = chooseTeamsMenu(allTeams);
        System.out.print("Enter a Board name: ");
        String name = scanner.nextLine();
        checkForName(allBoards, name);
        BoardImpl board = new BoardImpl(name);
        team.addBoard(name, board);
        return board;
    }

    MemberImpl createNewPerson() {
        Team team = chooseTeamsMenu(allTeams);
        System.out.print("Enter a Member name: ");
        String name = scanner.nextLine();
        checkForName(allMembers, name);
        MemberImpl member = new MemberImpl(name);
        team.addMember(name, member);
        return member;
    }

    WorkItem createWorkItem(String type) {
        Team team = chooseTeamsMenu(allTeams);
        BoardImpl board = chooseBoardsMenu(team.getBoards());
        MemberImpl member;
        WorkItem workItem;

        System.out.print("Title: ");
        String title = scanner.nextLine();
        System.out.print("Description: ");
        String description = scanner.nextLine();
        switch (type) {
            case BUG:
                BugStatus bugStatus = BugStatus.ACTIVE;
                System.out.println("Choose priority:");
                String bugPriority = chooseEnumMenu(PriorityLevel.values());
                System.out.println("Choose severity:");
                String severity = chooseEnumMenu(Severity.values());
                System.out.println("Choose an assignee: ");
                member = chooseMemberMenu(team.getMembers());
                workItem = new BugImpl(title, description, bugStatus,
                        Severity.getSeverity(severity), PriorityLevel.getPriority(bugPriority), member);
                member.addActivity(workItem);
                break;
            case STORY:
                StoryStatus storyStatus = StoryStatus.NOTDONE;
                System.out.println("Choose priority:");
                String storyPriority = chooseEnumMenu(PriorityLevel.values());
                System.out.println("Choose size:");
                String size = chooseEnumMenu(Size.values());
                System.out.println("Choose an assignee: ");
                member = chooseMemberMenu(team.getMembers());
                workItem = new StoryImpl(title, description, storyStatus
                        , Size.getSize(size), PriorityLevel.getPriority(storyPriority), member);
                member.addActivity(workItem);
                break;
            case FEEDBACK:
                FeedbackStatus feedbackStatus = FeedbackStatus.NEW;
                int rating = setRatingMenu();
                workItem = new FeedbackImpl(title, description, feedbackStatus, rating);
                break;
            default:
                throw new IllegalArgumentException();
        }
        board.addActivity(workItem);

        return workItem;
    }

    String getAllTeamBoards() {
        Team team = chooseTeamsMenu(allTeams);
        return getAllKeysFromMap(team.getBoards());
    }

    String getAllTeamMembers() {
        Team team = chooseTeamsMenu(allTeams);
        return getAllKeysFromMap(team.getMembers());
    }

    String getPersonActivity() {
        Team team = chooseTeamsMenu(allTeams);
        MemberImpl member = chooseMemberMenu(team.getMembers());
        return listToString(member.getActivity());

    }

    String getBoardActivity() {
        Team team = chooseTeamsMenu(allTeams);
        BoardImpl board = chooseBoardsMenu(team.getBoards());
        return listToString(board.getActivity());
    }

    String getTeamsActivity() {
        StringBuilder stringBuilder = new StringBuilder();
        Team team = chooseTeamsMenu(allTeams);
        for (Map.Entry<String, BoardImpl> entry : team.getBoards().entrySet()) {
            stringBuilder.append(listToString(entry.getValue().getActivity()));
            stringBuilder.append(System.lineSeparator());
        }
        return stringBuilder.toString();
    }

    void addCommentToAWorkItem() {
        Team team = chooseTeamsMenu(allTeams);
        BoardImpl board = chooseBoardsMenu(team.getBoards());
        WorkItem workItem = chooseWorkItemMenu(board.getActivity());
        System.out.println("Who is giving the comment?");
        MemberImpl member = chooseMemberMenu(allMembers);
        System.out.print("Write a comment: ");
        String comment = scanner.nextLine();
        workItem.addComments(member.getName() + " said: " + comment);
    }

    void changeWorkItemAssignee() {
        Team team = chooseTeamsMenu(allTeams);
        BoardImpl board = chooseBoardsMenu(team.getBoards());
        int choiceInt = chooseByIndex(board.getActivity());
        WorkItem workItem = board.getActivity().get(choiceInt);
        while (workItem instanceof Feedback) {
            System.out.println("Feedback has no Assignee!\n" +
                    "Please choose an Assignable Work Item!\n" +
                    "Click 'Enter' to continue...");
            scanner.nextLine();
            choiceInt = chooseByIndex(board.getActivity());
            workItem = board.getActivity().get(choiceInt);
        }
        MemberImpl member = chooseMemberMenu(team.getMembers());
        String history = ((Assignable) workItem).getAssignee().getName();
        ((Assignable) workItem).setAssignee(member);
        workItem.addToHistory("Assignee " + history + " changed to " + ((Assignable) workItem).getAssignee().getName());
    }

    void changePriorityOfAWorkItem() {
        Team team = chooseTeamsMenu(allTeams);
        BoardImpl board = chooseBoardsMenu(team.getBoards());
        WorkItem workItem = chooseWorkItemMenu(board
                .getActivity()
                .stream()
                .filter(workItem1 -> (workItem1 instanceof PriorityItemBase))
                .collect(Collectors.toList()));
        String history = ((PriorityItemBase) workItem).getPriority().toString();
        String choice = chooseEnumMenu(PriorityLevel.LOW, PriorityLevel.MEDIUM, PriorityLevel.HIGH);
        ((PriorityItemBase) workItem).setPriority(PriorityLevel.getPriority(choice));
        workItem.addToHistory("Priority Level " + history + " changed to " + ((PriorityItemBase) workItem).getPriority());
    }

    void changeStatusOfAWorkItem() {
        Team team = chooseTeamsMenu(allTeams);
        BoardImpl board = chooseBoardsMenu(team.getBoards());
        WorkItem workItem = chooseWorkItemMenu(board.getActivity());
        String history = workItem.getStatus().toString();
        String choice = chooseStatusOfWorkItem(workItem);
        workItem.setStatus(workItem.getStatus().getStatus(choice));
        workItem.addToHistory("Status " + history + " changed to " + workItem.getStatus());
    }

    void addStepsOfReproduction() {
        Team team = chooseTeamsMenu(allTeams);
        BoardImpl board = chooseBoardsMenu(team.getBoards());
        WorkItem workItem = chooseWorkItemMenu(board
                .getActivity()
                .stream()
                .filter(workItem1 -> (workItem1 instanceof BugImpl))
                .collect(Collectors.toList()));
        System.out.println("Enter step count: ");
        int stepCount = scanner.nextInt();
        String step;
        scanner.nextLine();
        for (int i = 0; i < stepCount; i++) {
            System.out.print(i + 1 + ". ");
            step = scanner.nextLine();
            ((BugImpl) workItem).addStepsToReproduce(step);
        }
    }

    void changeSeverityOfABug() {
        Team team = chooseTeamsMenu(allTeams);
        BoardImpl board = chooseBoardsMenu(team.getBoards());
        WorkItem workItem = chooseWorkItemMenu(board
                .getActivity()
                .stream()
                .filter(workItem1 -> (workItem1 instanceof BugImpl))
                .collect(Collectors.toList()));
        String history = ((BugImpl) workItem).getSeverity().toString();
        String choice = chooseEnumMenu(Severity.values());
        ((BugImpl) workItem).setSeverity(Severity.getSeverity(choice));
        workItem.addToHistory("Severity " + history + " changed to " + ((BugImpl) workItem).getSeverity().toString());
    }

    void changeSizeOfAStory() {
        Team team = chooseTeamsMenu(allTeams);
        BoardImpl board = chooseBoardsMenu(team.getBoards());
        WorkItem workItem = chooseWorkItemMenu(board
                .getActivity()
                .stream()
                .filter(workItem1 -> (workItem1 instanceof StoryImpl))
                .collect(Collectors.toList()));
        String history = ((StoryImpl) workItem).getSize().toString();
        String choice = chooseEnumMenu(Size.values());
        ((StoryImpl) workItem).setSize(Size.getSize(choice));
        workItem.addToHistory("Size " + history + " changed to " + ((StoryImpl) workItem).getSize().toString());
    }

    void changeRatingOfAFeedback() {
        Team team = chooseTeamsMenu(allTeams);
        BoardImpl board = chooseBoardsMenu(team.getBoards());
        WorkItem workItem = chooseWorkItemMenu(board
                .getActivity()
                .stream()
                .filter(workItem8 -> (workItem8 instanceof Feedback))
                .collect(Collectors.toList()));
        String history = Integer.toString(((FeedbackImpl) workItem).getRating());
        System.out.print("New Rating: ");
        int number = Integer.parseInt(scanner.nextLine());
        ((FeedbackImpl) workItem).setRating(number);
        workItem.addToHistory("Rating " + history + " changed to " + ((FeedbackImpl) workItem).getRating());
    }

    //ask for filters and sort, and return list of workItems
    String ListAndFilterWorkItems() {
        List<WorkItem> filteredWorkItems = new ArrayList<>();
        for (Map.Entry<Integer, WorkItem> entry : allWorkItems.entrySet()) {
            filteredWorkItems.add(entry.getValue());
        }
        boolean filteringAssignables = false;
        if (askForFilterBy("Member")) {
            filteringAssignables = true;
            MemberImpl member = chooseMemberMenu(allMembers);
            filteredWorkItems = filterWorkItemsByType(ASSIGNABLE, filteredWorkItems);
            filteredWorkItems = filteredWorkItems
                    .stream()
                    .filter(workItem -> ((Assignable) workItem).getAssignee().equals(member))
                    .collect(Collectors.toList());
        }
        if (filteredWorkItems.size() == 0) {
            return "No Elements!";
        }

        String typeFilter;
        if (askForFilterBy("WorkItem Type")) {
            typeFilter = chooseWorkItemType(filteringAssignables);
            filteredWorkItems = filterWorkItemsByType(typeFilter, filteredWorkItems);
        } else {
            typeFilter = ALL;
        }

        if (filteredWorkItems.size() == 0) {
            return "No Elements!";
        }

        if (!typeFilter.equals(ALL)) {
            if (askForFilterBy("Status")) {
                filteredWorkItems = filterWorkItemsByStatus(filteredWorkItems);
            }
        }
        if (filteredWorkItems.size() == 0) {
            return "No Elements!";
        }

        System.out.println("Sort by: ");
        String choice = chooseSortForWorkItem(typeFilter);
        sortWorkItemBy(filteredWorkItems, choice);

        return listToString(filteredWorkItems);
    }

    //sort list by the given parameter
    private void sortWorkItemBy(List<WorkItem> filteredWorkItems, String choice) {
        switch (choice) {
            case TITLE:
                filteredWorkItems.sort(Comparator.comparing(WorkItem::getTitle));
                break;
            case PRIORITY:
                filteredWorkItems.sort(Comparator.comparing(o -> ((Priority) o).getPriority()));
                break;
            case SEVERITY:
                filteredWorkItems.sort(Comparator.comparing(o -> ((Bug) o).getSeverity()));
                break;
            case SIZE:
                filteredWorkItems.sort(Comparator.comparing(o -> ((Story) o).getSize()));
                break;
            case RATING:
                filteredWorkItems.sort(Comparator.comparingInt(o -> ((Feedback) o).getRating()));
                break;
        }
    }

    //give us list with possible sort types and requires choice
    private String chooseSortForWorkItem(String typeOfWorkItems) {
        int choice = 0;
        ArrayList<String> sortTypes = new ArrayList<>();
        sortTypes.add(TITLE);
        switch (typeOfWorkItems) {
            case BUG:
                sortTypes.add(PRIORITY);
                sortTypes.add(SEVERITY);
                choice = chooseByIndex(sortTypes);
                break;
            case STORY:
                sortTypes.add(PRIORITY);
                sortTypes.add(SIZE);
                choice = chooseByIndex(sortTypes);
                break;
            case FEEDBACK:
                sortTypes.add(RATING);
                choice = chooseByIndex(sortTypes);
                break;
            default:
                break;
        }
        return sortTypes.get(choice);
    }

    //filter the given list by status
    private List<WorkItem> filterWorkItemsByStatus(List<WorkItem> filteredWorkItems) {

        String status = chooseStatusOfWorkItem(filteredWorkItems.get(0));
        return filteredWorkItems
                .stream()
                .filter(workItem -> workItem.getStatus().toString().equals(status.toUpperCase()))
                .collect(Collectors.toList());
    }

    //filter the given list by the given workItem type
    private List<WorkItem> filterWorkItemsByType(String typeFilter, List<WorkItem> filteredWorkItems) {
        switch (typeFilter) {
            case ASSIGNABLE:
                return filteredWorkItems
                        .stream()
                        .filter(workItem -> workItem instanceof Assignable)
                        .collect(Collectors.toList());
            case BUG:
                return filteredWorkItems
                        .stream()
                        .filter(workItem -> workItem instanceof Bug)
                        .collect(Collectors.toList());
            case STORY:
                return filteredWorkItems
                        .stream()
                        .filter(workItem -> workItem instanceof Story)
                        .collect(Collectors.toList());
            case FEEDBACK:
                return filteredWorkItems
                        .stream()
                        .filter(workItem -> workItem instanceof Feedback)
                        .collect(Collectors.toList());
            default:
                return new ArrayList<>();
        }
    }

    //print a question for filter type and return true if want this filter
    private boolean askForFilterBy(String filterBy) {
        while (true) {
            System.out.println(String.format("Do you want a filter by %s?", filterBy));
            System.out.println("You have to type Y(for Yes) or N(for No)!");
            System.out.print("Choice: ");
            String choice = scanner.nextLine();
            switch (choice.toUpperCase()) {
                case "Y":
                    return true;
                case "N":
                    return false;
                default:
                    System.out.println("Invalid command!");
                    break;
            }
        }
    }

    //print all workItems type and requires choice
    private String chooseWorkItemType(boolean filteringAssignables) {
        ArrayList<String> types = new ArrayList<>();
        types.add(BUG);
        types.add(STORY);
        if (!filteringAssignables) {
            types.add(FEEDBACK);
        }
        int choice = chooseByIndex(types);
        switch (types.get(choice)) {
            case BUG:
                return BUG;
            case STORY:
                return STORY;
            case FEEDBACK:
                return FEEDBACK;
            default:
                return "Invalid WorkItem!";
        }
    }

    //print all possible status for the given workItem and require choice
    private String chooseStatusOfWorkItem(WorkItem workItem) {
        if (workItem instanceof Bug) {
            return chooseEnumMenu(BugStatus.values());
        } else if (workItem instanceof Story) {
            return chooseEnumMenu(StoryStatus.values());
        } else if (workItem instanceof Feedback) {
            return chooseEnumMenu(FeedbackStatus.values());
        } else {
            return "Invalid Status!";
        }
    }

    //wants input while the number is valid
    private int setRatingMenu() {
        int rating;
        while (true) {
            System.out.print("Rating: ");
            String pars = scanner.nextLine();
            if ((!pars.equals("")) && (pars.length() == pars.replaceAll("[^\\d]", "").length())) {
                if (Integer.parseInt(pars) > 0) {
                    rating = Integer.parseInt(pars);
                    return rating;
                }
            }
            System.out.println("It is not valid rating!");
        }
    }

    //check all work Items from the given list and if list are empty throw exception else we have menu to choose our wWrk Item
    private WorkItem chooseWorkItemMenu(List<WorkItem> arr) {
        if (arr.size() == 0) {
            throw new IllegalArgumentException("You have to add WorkItem in a board first!");
        }
        int choice = chooseByIndex(arr);
        return arr.get(choice);
    }

    //check all members from the given list and if list are empty throw exception else we have menu to choose our member
    private MemberImpl chooseMemberMenu(Map<String, MemberImpl> map) {
        if (map.size() == 0) {
            throw new IllegalArgumentException("You have to add Member in a team first!");
        }
        List<String> list = new ArrayList<>();
        System.out.println("Choose a Member: ");
        for (Map.Entry<String, MemberImpl> entry : map.entrySet()) {
            list.add(entry.getKey());
        }
        int choice = chooseByIndex(list);
        return map.get(list.get(choice));
    }

    //check all boards from the given list and if list are empty throw exception else we have menu to choose our board
    private BoardImpl chooseBoardsMenu(Map<String, BoardImpl> map) {
        if (map.size() == 0) {
            throw new IllegalArgumentException("You have to create Board first!");
        }
        List<String> list = new ArrayList<>();
        System.out.println("Choose a Board: ");
        for (Map.Entry<String, BoardImpl> entry : map.entrySet()) {
            list.add(entry.getKey());
        }
        int choice = chooseByIndex(list);
        return map.get(list.get(choice));
    }

    //check all teams and if we have no teams throw exception else we have menu to choose our team
    private Team chooseTeamsMenu(Map<String, Team> map) {
        if (map.size() == 0) {
            throw new IllegalArgumentException("You have to create Team first!");
        }
        List<String> list = new ArrayList<>();
        System.out.println("Choose a Team: ");
        for (Map.Entry<String, Team> entry : map.entrySet()) {
            list.add(entry.getKey());
        }
        int choice = chooseByIndex(list);
        return map.get(list.get(choice));
    }

    //display all introduced enums while we introduce valid String for enum getters
    private String chooseEnumMenu(EnumCollection... allEnums) {
        List<String> list = new ArrayList<>();
        for (EnumCollection enumType : allEnums) {
            list.add(enumType.toString());
        }
        int choice = chooseByIndex(list);
        return list.get(choice);
    }

    //display indexes and objects while we introduce valid index
    int chooseByIndex(List list) {
        while (true) {
            System.out.println(listToString(list));
            System.out.print("Enter a [number] of your choice: ");
            String string = scanner.nextLine();
            if ((!string.equals("")) && (string.length() == string.replaceAll("[^\\d]", "").length())) {
                int choice = Integer.parseInt(string);
                if (choice <= list.size() && choice > 0) {
                    return choice - 1;
                }
            }
            System.out.println("Invalid number!");
        }
    }

    //check all member names and if already exist member with this name throw exception
    private void checkForName(Map map, String name) {
        if (map.containsKey(name)) {
            throw new IllegalArgumentException(String.format("Name '%s' already exist!", name));
        }
    }

    //print the given list and append indices
    String listToString(List list) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            stringBuilder.append("[").append(i + 1).append("] ").append(list.get(i));
            stringBuilder.append(System.lineSeparator());
        }
        return stringBuilder.toString();
    }

    //return string which contains all keys from given map and indexes
    String getAllKeysFromMap(Map map) {
        int counter = 1;
        StringBuilder stringBuilder = new StringBuilder();
        Iterator it = map.entrySet().iterator();
        if (!it.hasNext()) {
            return "No elements!";
        }
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            stringBuilder.append("[").append(counter++).append("] ").append(pair.getKey()).append(" ");
            stringBuilder.append(System.lineSeparator());
        }
        return stringBuilder.toString();
    }
}
