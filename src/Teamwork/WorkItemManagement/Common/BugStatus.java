package Teamwork.WorkItemManagement.Common;

public enum BugStatus implements Status{
    ACTIVE, FIXED;

    public BugStatus getStatus(String status) {
        switch (status.toUpperCase()) {
            case "ACTIVE":
                return BugStatus.ACTIVE;
            case "FIXED":
                return BugStatus.FIXED;
            default:
                throw new IllegalArgumentException();
        }
    }
}
