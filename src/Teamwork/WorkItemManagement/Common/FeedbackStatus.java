package Teamwork.WorkItemManagement.Common;

public enum FeedbackStatus implements Status{
    NEW, UNSCHEDULED, SCHEDULED;

    public FeedbackStatus getStatus(String status) {
        switch (status.toUpperCase()) {
            case "NEW":
                return FeedbackStatus.NEW;
            case "SCHEDULED":
                return FeedbackStatus.SCHEDULED;
            case "UNSCHEDULED":
                return FeedbackStatus.UNSCHEDULED;
            default:
                throw new IllegalArgumentException();
        }
    }
}

