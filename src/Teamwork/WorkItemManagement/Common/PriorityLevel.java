package Teamwork.WorkItemManagement.Common;

public enum PriorityLevel implements EnumCollection {
    HIGH, MEDIUM, LOW;



    public static PriorityLevel getPriority(String priority) {
        switch (priority.toUpperCase()) {
            case "HIGH":
                return PriorityLevel.HIGH;
            case "MEDIUM":
                return PriorityLevel.MEDIUM;
            case "LOW":
                return PriorityLevel.LOW;
            default:
                throw new IllegalArgumentException();
        }
    }

}
