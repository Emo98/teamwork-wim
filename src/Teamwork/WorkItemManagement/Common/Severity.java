package Teamwork.WorkItemManagement.Common;

public enum Severity implements EnumCollection {
    CRITICAL, MAJOR, MINOR;


    public static Severity getSeverity(String severity) {
        switch (severity.toUpperCase()) {
            case "CRITICAL":
                return Severity.CRITICAL;
            case "MAJOR":
                return Severity.MAJOR;
            case "MINOR":
                return Severity.MINOR;
            default:
                throw new IllegalArgumentException();
        }
    }
}
