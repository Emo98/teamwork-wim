package Teamwork.WorkItemManagement.Common;

public enum Size implements EnumCollection {
    LARGE, MEDIUM, SMALL;


    public static Size getSize(String size) {
        switch (size.toUpperCase()) {
            case "LARGE":
                return Size.LARGE;
            case "MEDIUM":
                return Size.MEDIUM;
            case "SMALL":
                return Size.SMALL;
            default:
                throw new IllegalArgumentException();
        }
    }
}

