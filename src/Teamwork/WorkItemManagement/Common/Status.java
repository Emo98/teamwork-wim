package Teamwork.WorkItemManagement.Common;

public interface Status extends EnumCollection {

    Status getStatus(String status);
}
