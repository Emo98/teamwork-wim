package Teamwork.WorkItemManagement.Common;

public enum StoryStatus implements Status{
    NOTDONE, INPROGRESS, DONE;

    public StoryStatus getStatus(String status) {
        switch (status.toUpperCase()) {
            case "NOTDONE":
                return StoryStatus.NOTDONE;
            case "DONE":
                return StoryStatus.DONE;
            case "INPROGRESS":
                return StoryStatus.INPROGRESS;
            default:
                throw new IllegalArgumentException();
        }
    }
}
