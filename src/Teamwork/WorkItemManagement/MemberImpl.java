package Teamwork.WorkItemManagement;

public class MemberImpl extends TeamItemBase {
    private static final int MIN_NAME_LENGTH = 5;
    private static final int MAX_NAME_LENGTH = 15;

    public MemberImpl(String name) {
        super(name);
    }

    @Override
    public int getMaxNameLength() {
        return MAX_NAME_LENGTH;
    }

    @Override
    public int getMinNameLength() {
        return MIN_NAME_LENGTH;
    }


}
