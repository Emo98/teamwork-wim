package Teamwork.WorkItemManagement;

import Teamwork.WorkItemManagement.contracts.Team;

import java.util.HashMap;

public class TeamImpl implements Team {

    private String name;
    private HashMap<String, MemberImpl> members;
    private HashMap<String, BoardImpl> boards;

    public TeamImpl(String name) {
        setName(name);
        members = new HashMap<>();
        boards = new HashMap<>();
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    public HashMap<String, BoardImpl> getBoards() {
        return new HashMap<>(boards);
    }

    public HashMap<String, MemberImpl> getMembers() {
        return new HashMap<>(members);
    }

    public void addMember(String name, MemberImpl member) {
        members.put(name, member);
    }

    public void addBoard(String name, BoardImpl board) {
        boards.put(name, board);
    }

    public boolean isReadyForWork() {
        return getBoards().size() > 0 && getMembers().size() > 0;
    }

    @Override
    public String toString() {
        return "{" + "Members:" + members + '}';
    }
}
