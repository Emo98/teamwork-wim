package Teamwork.WorkItemManagement;

import Teamwork.WorkItemManagement.WorkItems.contracts.WorkItem;
import Teamwork.WorkItemManagement.contracts.Activity;

import java.util.ArrayList;
import java.util.Objects;

public abstract class TeamItemBase implements Activity {


    private static final String NAME_VALIDATION_MESSAGE = "Name should be between %d and %s symbols.";
    private String name;
    private ArrayList<WorkItem> activity;

    TeamItemBase(String name) {
        setName(name);
        activity = new ArrayList<>();
    }

    private void setName(String name) {
        if (name.length() < getMinNameLength() || name.length() > getMaxNameLength()) {
            throw new IllegalArgumentException(String.format(
                    NAME_VALIDATION_MESSAGE, getMinNameLength(), getMaxNameLength())
            );
        }
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public ArrayList<WorkItem> getActivity() {
        return new ArrayList<>(activity);
    }

    public abstract int getMinNameLength();

    public abstract int getMaxNameLength();


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TeamItemBase that = (TeamItemBase) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public static void checkString(String string, int minLength, int maxLength) {
        if (string == null) {
            throw new IllegalArgumentException("Title/Description can not be NULL!");
        }
        if (string.length() < minLength || string.length() > maxLength) {
            throw new IllegalArgumentException(String.format
                    ("Title/Description must be between %d and %d symbols!", minLength, maxLength)
            );
        }
    }

    public void addActivity(WorkItem workItem) {
        activity.add(workItem);
    }

    @Override
    public String toString() {
        return "Name: " + name + " ";
    }
}
