package Teamwork.WorkItemManagement.WorkItems;

import Teamwork.WorkItemManagement.Common.BugStatus;
import Teamwork.WorkItemManagement.Common.PriorityLevel;
import Teamwork.WorkItemManagement.Common.Severity;
import Teamwork.WorkItemManagement.Common.Status;
import Teamwork.WorkItemManagement.MemberImpl;
import Teamwork.WorkItemManagement.WorkItems.contracts.Bug;

import java.lang.reflect.Member;
import java.util.ArrayList;

public class BugImpl extends PriorityItemBase implements Bug {

    private static final String WORKITEM_TYPE = "BUG";
    private ArrayList<String> stepsToReproduce;
    private Severity severity;


    public BugImpl(String title, String description, BugStatus status, Severity severity, PriorityLevel priority, MemberImpl assignee) {
        super(title, description, status, priority, assignee);
        setSeverity(severity);
        stepsToReproduce = new ArrayList<>();
    }

    public ArrayList<String> getStepsToReproduce() {
        return new ArrayList<>(stepsToReproduce);
    }

    public void addStepsToReproduce(String step) {
        stepsToReproduce.add(step);
    }

    public Severity getSeverity() {
        return severity;
    }

    public void setSeverity(Severity severity) {
        this.severity = severity;
    }


    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(super.toString())
                .append("Severity: ").append(getSeverity()).append('\n')
                .append("Steps to reproduce: ").append('\n');
        for (int i = 0; i < stepsToReproduce.size(); i++) {
            stringBuilder.append(i+1).append(". ").append(stepsToReproduce.get(i));
            stringBuilder.append(System.lineSeparator());
        }
        return stringBuilder.toString();
    }

    @Override
    protected String getType() {
        return WORKITEM_TYPE;
    }
}
