package Teamwork.WorkItemManagement.WorkItems;

import Teamwork.WorkItemManagement.Common.FeedbackStatus;
import Teamwork.WorkItemManagement.Common.Status;
import Teamwork.WorkItemManagement.MemberImpl;
import Teamwork.WorkItemManagement.WorkItems.contracts.Feedback;

public class FeedbackImpl extends WorkItemBase implements Feedback {

    private final String WORKITEM_TYPE = "FEEDBACK";
    private int rating;

    public FeedbackImpl(String title, String description, FeedbackStatus status, int rating) {
        super(title, description, status);
        setRating(rating);
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getRating() {
        return rating;
    }

    @Override
    public String toString() {
        return super.toString() +
                "Rating: " + getRating();
    }

    @Override
    protected String getType() {
        return WORKITEM_TYPE;
    }
}
