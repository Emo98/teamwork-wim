package Teamwork.WorkItemManagement.WorkItems;

import Teamwork.WorkItemManagement.Common.PriorityLevel;
import Teamwork.WorkItemManagement.Common.Status;
import Teamwork.WorkItemManagement.MemberImpl;
import Teamwork.WorkItemManagement.WorkItems.contracts.Assignable;
import Teamwork.WorkItemManagement.WorkItems.contracts.Priority;

public abstract class PriorityItemBase extends WorkItemBase implements Priority, Assignable {

    private PriorityLevel priority;
    private MemberImpl assignee;

    PriorityItemBase(String title, String description, Status status, PriorityLevel priority, MemberImpl assignee) {
        super(title, description, status);
        setPriority(priority);
        setAssignee(assignee);
    }

    public PriorityLevel getPriority() {
        return priority;
    }

    public void setPriority(PriorityLevel priority) {
        this.priority = priority;
    }

    @Override
    public void setAssignee(MemberImpl assignee) {
        this.assignee = assignee;
    }

    @Override
    public MemberImpl getAssignee() {
        return assignee;
    }

    @Override
    public String toString() {
        return super.toString() +
                "Assignee: " + getAssignee().getName() + '\n' +
                "Priority Level: " + getPriority() + '\n';
    }
}
