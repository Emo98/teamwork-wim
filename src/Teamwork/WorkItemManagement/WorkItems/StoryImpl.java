package Teamwork.WorkItemManagement.WorkItems;

import Teamwork.WorkItemManagement.Common.*;
import Teamwork.WorkItemManagement.MemberImpl;
import Teamwork.WorkItemManagement.WorkItems.contracts.Story;

public class StoryImpl extends PriorityItemBase implements Story {

    private static final String WORKITEM_TYPE = "STORY";
    private Size size;

    public StoryImpl(String title, String description, StoryStatus status, Size size, PriorityLevel priority, MemberImpl assignee) {
        super(title, description, status, priority, assignee);
        setSize(size);
}

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return super.toString() +
                "Size: " + getSize();
    }

    @Override
    protected String getType() {
        return WORKITEM_TYPE;
    }
}
