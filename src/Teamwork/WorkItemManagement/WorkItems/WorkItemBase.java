package Teamwork.WorkItemManagement.WorkItems;

import Teamwork.WorkItemManagement.TeamItemBase;
import Teamwork.WorkItemManagement.Common.Status;
import Teamwork.WorkItemManagement.MemberImpl;
import Teamwork.WorkItemManagement.WorkItems.contracts.WorkItem;

import java.util.ArrayList;

public abstract class WorkItemBase implements WorkItem {

    private static final int MIN_TITLE_LENGTH = 10;
    private static final int MAX_TITLE_LENGTH = 50;
    private static final int MIN_DESCRIPTION_LENGTH = 10;
    private static final int MAX_DESCRIPTION_LENGTH = 500;

    private static int uniqueId = 1;

    private final int id;
    private String title;
    private String description;
    private Status status;
    private ArrayList<String> comments;
    private ArrayList<String> history;

    WorkItemBase(String title, String description, Status status) {
        this.id = uniqueId++;
        this.comments = new ArrayList<>();
        this.history = new ArrayList<>();
        setTitle(title);
        setDescription(description);
        setStatus(status);
    }


    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    private void setTitle(String title) {
        TeamItemBase.checkString(title, MIN_TITLE_LENGTH, MAX_TITLE_LENGTH);
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    private void setDescription(String description) {
        TeamItemBase.checkString(description, MIN_DESCRIPTION_LENGTH, MAX_DESCRIPTION_LENGTH);
        this.description = description;
    }

    public ArrayList<String> getComments() {
        return new ArrayList<>(comments);
    }

    public ArrayList<String> getHistory() {
        return new ArrayList<>(history);
    }

    public void addComments(String comment) {
        comments.add(comment);
    }

    @Override
    public void addToHistory(String history) {
        this.history.add(history);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\nID: ").append(getId()).append('\n');
        stringBuilder.append("Type: ").append(getType()).append('\n');
        stringBuilder.append("Title: ").append(getTitle()).append('\n')
                .append("Description: ").append(getDescription()).append('\n')
                .append("Status: ").append(getStatus()).append('\n')
                .append("Comments: ").append('\n');
        for (int i = 0; i < getComments().size(); i++) {
            stringBuilder.append(getComments().get(i));
            stringBuilder.append(System.lineSeparator());
        }
        stringBuilder.append("History: ").append('\n');
        for (int i = 0; i < getHistory().size(); i++) {
            stringBuilder.append(i+1).append(". ").append(getHistory().get(i));
            stringBuilder.append(System.lineSeparator());
        }
        return stringBuilder.toString();
    }

    protected abstract String getType();
}
