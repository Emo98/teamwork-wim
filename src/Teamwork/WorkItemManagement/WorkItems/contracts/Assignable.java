package Teamwork.WorkItemManagement.WorkItems.contracts;


import Teamwork.WorkItemManagement.MemberImpl;

public interface Assignable {
    void setAssignee(MemberImpl member);

    MemberImpl getAssignee();

}
