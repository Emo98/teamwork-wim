package Teamwork.WorkItemManagement.WorkItems.contracts;

import Teamwork.WorkItemManagement.Common.Severity;
import Teamwork.WorkItemManagement.Common.Status;

import java.util.ArrayList;

public interface Bug extends Assignable, WorkItem{

    ArrayList<String> getStepsToReproduce();

    Severity getSeverity();
}
