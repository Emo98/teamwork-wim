package Teamwork.WorkItemManagement.WorkItems.contracts;

public interface Feedback extends WorkItem{
    void setRating(int rating);

    int getRating();
}
