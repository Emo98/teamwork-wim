package Teamwork.WorkItemManagement.WorkItems.contracts;

import Teamwork.WorkItemManagement.Common.PriorityLevel;

public interface Priority extends WorkItem{

    PriorityLevel getPriority();

    void setPriority(PriorityLevel priority);
}
