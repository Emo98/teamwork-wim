package Teamwork.WorkItemManagement.WorkItems.contracts;

import Teamwork.WorkItemManagement.Common.Size;

public interface Story extends Assignable, WorkItem{
    Size getSize();

    void setSize(Size size);
}
