package Teamwork.WorkItemManagement.WorkItems.contracts;

import Teamwork.WorkItemManagement.Common.Status;
import Teamwork.WorkItemManagement.MemberImpl;

import java.util.ArrayList;

public interface WorkItem {
   Status getStatus();

   void setStatus(Status status);

   int getId() ;

   String getTitle();

   String getDescription();

   ArrayList<String> getComments();

   ArrayList<String> getHistory();

   void addComments(String comment);

   void addToHistory(String history);

}
