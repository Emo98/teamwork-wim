package Teamwork.WorkItemManagement.contracts;

import Teamwork.WorkItemManagement.WorkItems.contracts.WorkItem;

import java.util.ArrayList;

public interface Activity {

    ArrayList<WorkItem> getActivity();
}
