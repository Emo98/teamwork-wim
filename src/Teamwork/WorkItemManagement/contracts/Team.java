package Teamwork.WorkItemManagement.contracts;

import Teamwork.WorkItemManagement.BoardImpl;
import Teamwork.WorkItemManagement.MemberImpl;

import java.util.HashMap;

public interface Team {
    String getName();

    HashMap<String, BoardImpl> getBoards();

    HashMap<String, MemberImpl> getMembers();

    void addMember(String name, MemberImpl member);

    void addBoard(String name, BoardImpl board);

    boolean isReadyForWork();
}
