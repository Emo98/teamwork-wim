package Tests;

import Teamwork.WorkItemManagement.BoardImpl;
import Teamwork.WorkItemManagement.Common.*;
import Teamwork.WorkItemManagement.MemberImpl;
import Teamwork.WorkItemManagement.TeamImpl;
import Teamwork.WorkItemManagement.WorkItems.BugImpl;
import Teamwork.WorkItemManagement.WorkItems.FeedbackImpl;
import Teamwork.WorkItemManagement.WorkItems.StoryImpl;
import Teamwork.WorkItemManagement.WorkItems.contracts.Bug;
import Teamwork.WorkItemManagement.WorkItems.contracts.Feedback;
import Teamwork.WorkItemManagement.WorkItems.contracts.Story;
import Teamwork.WorkItemManagement.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ConstructorTests {
    private Team team;
    private MemberImpl member;
    private BoardImpl board;
    private Bug bug;
    private Story story;
    private Feedback feedback;

    @Before
    public void initAllFields() {
        team = new TeamImpl("BestTeam");
        member = new MemberImpl("Daniel");
        board = new BoardImpl("Trello");
        bug = new BugImpl("This is a Title", "Description...", BugStatus.ACTIVE, Severity.CRITICAL, PriorityLevel.HIGH, member);
        story = new StoryImpl("This is a Title", "Description...", StoryStatus.NOTDONE, Size.LARGE, PriorityLevel.LOW, member);
        feedback = new FeedbackImpl("This is a Title", "Description...", FeedbackStatus.SCHEDULED, 7);
    }

    @Test
    public void teamConstructor_Should() {
        if (!team.getName().equals("BestTeam")) {
            Assert.fail();
        }
    }

    @Test
    public void memberConstructor_Should() {
        if (!member.getName().equals("Daniel")) {
            Assert.fail();
        }
    }

    @Test
    public void boardConstructor_Should() {
        if (!board.getName().equals("Trello")) {
            Assert.fail();
        }
    }

    @Test
    public void bugConstructor_Should() {
        if (!bug.getTitle().equals("This is a Title")) {
            Assert.fail();
        }
        if (!bug.getDescription().equals("Description...")) {
            Assert.fail();
        }
        if (!bug.getStatus().equals(BugStatus.ACTIVE)) {
            Assert.fail();
        }
        if (!bug.getSeverity().equals(Severity.CRITICAL)) {
            Assert.fail();
        }
        if (bug.getAssignee() != member) {
            Assert.fail();
        }
    }

    @Test
    public void storyConstructor_Should() {
        if (!story.getTitle().equals("This is a Title")) {
            Assert.fail();
        }
        if (!story.getDescription().equals("Description...")) {
            Assert.fail();
        }
        if (!story.getStatus().equals(StoryStatus.NOTDONE)) {
            Assert.fail();
        }
        if (!story.getSize().equals(Size.LARGE)) {
            Assert.fail();
        }
        if (story.getAssignee() != member) {
            Assert.fail();
        }
    }

    @Test
    public void feedbackConstructor_Should() {
        if (!feedback.getTitle().equals("This is a Title")) {
            Assert.fail();
        }
        if (!feedback.getDescription().equals("Description...")) {
            Assert.fail();
        }
        if (!feedback.getStatus().equals(FeedbackStatus.SCHEDULED)) {
            Assert.fail();
        }
        if (feedback.getRating() != 7) {
            Assert.fail();
        }
    }
}
