package Tests;

import Teamwork.WorkItemManagement.BoardImpl;
import Teamwork.WorkItemManagement.Common.*;
import Teamwork.WorkItemManagement.MemberImpl;
import Teamwork.WorkItemManagement.WorkItems.BugImpl;
import Teamwork.WorkItemManagement.WorkItems.FeedbackImpl;
import Teamwork.WorkItemManagement.WorkItems.StoryImpl;
import Teamwork.WorkItemManagement.WorkItems.contracts.Bug;
import Teamwork.WorkItemManagement.WorkItems.contracts.Feedback;
import Teamwork.WorkItemManagement.WorkItems.contracts.Story;
import org.junit.Test;

public class ValidationTests {
    private final static String smallDescription = "123456789"; // 9 symbols
    private final static String hugeDescription = "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345" +
            "6789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345" +
            "678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567" +
            "8901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890" +
            "12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890+"; // 501 symbols

    // Validation test units
    private MemberImpl member1;
    private BoardImpl board1;
    private Bug bug1;
    private Story story1;
    private Feedback feedback1;

    @Test(expected = IllegalArgumentException.class)
    public void memberNameMinLenTest() {
        member1 = new MemberImpl("1234");
    }

    @Test(expected = IllegalArgumentException.class)
    public void memberNameMaxLenTest() {
        member1 = new MemberImpl("123456789012345+");
    }

    @Test(expected = IllegalArgumentException.class)
    public void boardNameMinLenTest() {
        board1 = new BoardImpl("1234");
    }

    @Test(expected = IllegalArgumentException.class)
    public void boardNameMaxLenTest() {
        board1 = new BoardImpl("1234567890+");
    }

    @Test(expected = IllegalArgumentException.class)
    public void bugTitleMinLenTest() {
        String title = "123456789"; // 9 symbols
        bug1 = new BugImpl(title, "12345678901", BugStatus.ACTIVE, Severity.MINOR, PriorityLevel.HIGH, member1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void bugTitleMaxLenTest() {
        String title = "12345678901234567890123456789012345678901234567890+"; // 51 symbols
        bug1 = new BugImpl(title, "12345678901", BugStatus.ACTIVE, Severity.MINOR, PriorityLevel.HIGH, member1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void storyTitleMinLenTest() {
        String title = "123456789"; // 9 symbols
        story1 = new StoryImpl(title, "12345678901", StoryStatus.DONE, Size.LARGE, PriorityLevel.HIGH, member1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void storyTitleMaxLenTest() {
        String title = "12345678901234567890123456789012345678901234567890+"; // 51 symbols
        story1 = new StoryImpl(title, "12345678901", StoryStatus.DONE, Size.LARGE, PriorityLevel.HIGH, member1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void feedbackTitleMinLenTest() {
        String title = "123456789"; // 9 symbols
        feedback1 = new FeedbackImpl(title, "12345678901", FeedbackStatus.SCHEDULED, 10);
    }

    @Test(expected = IllegalArgumentException.class)
    public void feedbackTitleMaxLenTest() {
        String title = "12345678901234567890123456789012345678901234567890+"; // 51 symbols
        feedback1 = new FeedbackImpl(title, "12345678901", FeedbackStatus.SCHEDULED, 10);
    }

    @Test(expected = IllegalArgumentException.class)
    public void bugDescriptionMinLenTest() {
        bug1 = new BugImpl("12345678901", smallDescription, BugStatus.ACTIVE, Severity.MINOR, PriorityLevel.HIGH, member1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void bugDescriptionMaxLenTest() {
        bug1 = new BugImpl("12345678901", hugeDescription, BugStatus.ACTIVE, Severity.MINOR, PriorityLevel.HIGH, member1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void storyDescriptionMinLenTest() {
        story1 = new StoryImpl("12345678901", smallDescription, StoryStatus.DONE, Size.LARGE,PriorityLevel.HIGH, member1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void storyDescriptionMaxLenTest() {
        story1 = new StoryImpl("12345678901", hugeDescription, StoryStatus.DONE,Size.LARGE,PriorityLevel.HIGH, member1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void feedbackDescriptionMinLenTest() {
        feedback1 = new FeedbackImpl("12345678901", smallDescription, FeedbackStatus.SCHEDULED, 6);
    }

    @Test(expected = IllegalArgumentException.class)
    public void feedbackDescriptionMaxLenTest() {

        feedback1 = new FeedbackImpl("12345678901", hugeDescription, FeedbackStatus.SCHEDULED, 6);
    }
}
